function getRandomParagraph(node) {
  let pTags = node.getElementsByTagName("p");
  let randomIndex = Math.floor(Math.random() * pTags.length);
  console.log("Generated random paragraph: " + randomIndex);
  let result = pTags[randomIndex];
  return result.innerText;
}