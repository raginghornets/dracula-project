let canvas, ctx;

/**
 * Load the game.
 * @param {Scene} scene 
 */
function start(scene) {
  canvas = scene.load();
  canvas.id = "gameCanvas";
  canvas.tabIndex = 1;
  ctx = canvas.getContext("2d");
  document.getElementById("gameContainer").appendChild(canvas);  

  canvas.addEventListener("keydown", function (e) {
    keysDown[e.keyCode] = true;
  }, false);

  canvas.addEventListener("keyup", function (e) {
    delete keysDown[e.keyCode];
  }, false);
}

/**
 * Called when the website is loaded.
 */
function main() {
  start(mainScene);
  mainScene.setFps(100);
  mainScene.gameLoop(ctx);
}

main();