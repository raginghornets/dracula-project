let mainScene, player, player2;

mainScene = new Scene(500, 500);
mainScene.backgroundColor = 'gray';

player = new GameObject(id = "Player");
player.position = mainScene.center(player.scale);
player.speed = 10;
player.setImage("./assets/VampireHunter.gif");
player.score = 0;
mainScene.gameObjects.push(player);

playerScore = new TextObject("Score: " + player.score);
mainScene.textObjects.push(playerScore);

player2 = new GameObject(id = "Player2");
player2.setRandomPosition(mainScene);
player2.setImage("./assets/Vampire.png");
mainScene.gameObjects.push(player2);

player.update = function () {
  this.velocity = new Vector2(GetAxis("Horizontal"), GetAxis("Vertical"));
  this.position = this.position.add(this.velocity.multiply(this.speed));
  if (this.collider.collide(player2)) {
    this.score++;
    playerScore.text = "Score: " + player.score;
    player2.setRandomPosition(mainScene);
  }
};